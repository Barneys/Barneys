# 个人云服务器搭建git服务器


<!--more-->
1.创建`git`用户并赋予`root`权限

```sh
sudo adduser git # 输入用户名密码
sudo usermod -aG sudo git # 赋予sudo权限
```

在`root`用户下

2.创建`ssh`文件夹

```sh
cd
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

3.设置免密登录

在本地生成公钥

```sh
ssh-keygen 
```

将生成的公钥上传到第2步中创建的`.ssh/authorized_keys`文件中

在本地`~/.ssh/config`中写入下列内容

```sh
Host git_server
        HostName 公网ip
        User git
        IdentityFile C:\Users\admin\.ssh\git_rsa
```

测试免密登录

```sh
ssh git_server
```

4.在云服务器上新建仓库

创建一个`project`文件夹保存所有的仓库,`project/repo`:一个名为`repo`的仓库

```sh
ssh git_server
mkdir project && mkdir project/repo/
cd project/repo/
git init --bare repo.git
chmod -R 777 repo.git
# 设置仓库的owner为用户git:组名git(没必要做这一步)
# chown -R git:git repo.git
```

5.克隆仓库到本地

```sh
git clone git_server:/home/git/project/repo/repo.git
```

6.本地推送到远程仓库

进入上一步克隆下来的仓库文件夹操作

```sh
cd repo
vim 1.txt # 创建一个文件
git add -A
git commit -m 'first commit'
git config user.name your_git_name
git config user.email your_git_password
git push -u origin master
```



