# frp内网穿透记录


<!--more-->

### 1.环境介绍

- 腾讯轻量云服务器(**server1**)+宝塔面板
- 本地Ubuntu20.04LTS(**server2**)+Nginx+静态IP

### 2.安装frp工具

下载frp工具:[https://github.com/fatedier/frp/releases](https://github.com/fatedier/frp/releases)

<center>{{<image src="/images/all/image-20221012181354341.png" caption="">}}</center>

新建一个`frp`文件夹，解压上面下载的压缩包到该文件夹。

### 3.frp服务器端配置

本节都在`server1`操作, 将`server1`配置为`frp`服务器端

先将上面解压的文件夹上传到`server1`的`/root`路径

```sh
scp ./frp server1:/root
ssh server1
sudo cd /root
# 添加权限
chmod 777 frps.ini frps
```

`frp`服务器端配置在`frps.ini`中,配置如下

```ini
[common]
bind_port = 6000
vhost_http_port = 808
vhost_https_port = 909
dashboard_user = admin
dashboard_pwd = admin
token=yourpassword
```

**注: `6000`，`808`，`909`需要在`server1`中放行**

上述配置中`6000`是`frp`服务端口，`808`是`http`服务端口，`909`是`https`服务端口，由于我使用了宝塔面板，和`80`和`443`端口冲突了

启动服务器端

```sh
./frps -c frps.ini
```

我们也可以将`frps`配置为服务

首先创建一个服务文件: `frps.service`

```sh
sudo vim /etc/systemd/system/frps.service
chmod 777 /etc/systemd/system/frps.service
```

配置如下:

```
[Unit]
Dcription=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target
 
[Service]
Type=simple
User=root
Restart=on-failure
ExecStart=/root/frp/frps -c /root/frp/frps.ini
ExecReload=/root/frp/frps -c /root/frp/frps.ini
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true
StandardOutput=syslog
StandardError=inherit
 
[Install]
WantedBy=multi-user.target
```

添加该服务

```sh
# 通知systemd一个新的单元文件存在
sudo systemctl daemon-reload

# 应用frps服务
sudo systemctl enable --now frps
```

服务管理:

```sh
# 检查服务状态
sudo systemctl status frps
# 启动、停止、重启frps
sudo systemctl start frps
sudo systemctl stop frps
sudo systemctl restart frps
```

### 4.frp客户端配置

本节将`server2`配置为`frp`客户端

先将上面解压的文件夹上传到`server2`的`/root`路径

```sh
scp ./frp server2:/root
ssh server2
sudo cd /root
# 添加权限
chmod 777 frpc.ini frpc
```

`frp`客户端配置在`frpc.ini`中,配置如下

```ini
[common]
server_addr = 替换为server1的ip
server_port = 6000
tls_enable = true
token=yourpassword
 
[EXSI]
type = https
local_ip = 你的内网ip(如server2的ip)
local_port = 80
remote_port = 909
custom_domains = exsi.abc.com
 
[NAS]
type = http
local_ip = 你的内网ip(如server2的ip)
local_port = 5000
remote_port = 808
custom_domains = nas.abc.com
 
[localblog]
type = http
local_ip = 你的内网ip(如server2的ip)
local_port = 8888
remote_port = 808
custom_domains = localblog.taholab.com 
 
[ssh]
type = tcp
local_ip = 127.0.0.1
local_port = 22
remote_port = 6000
```

启动客户端

```sh
./frpc -c frpc.ini
```

同理，我们也可以将`frpc`配置为服务

首先创建一个服务文件: `frpc.service`

```sh
sudo vim /etc/systemd/system/frpc.service
chmod 777 /etc/systemd/system/frpc.service
```

配置如下:

```
[Unit]
Dcription=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target
 
[Service]
Type=simple
User=root
Restart=on-failure
ExecStart=/root/frp/frpc -c /root/frp/frpc.ini
ExecReload=/root/frp/frpc -c /root/frp/frpc.ini
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true
StandardOutput=syslog
StandardError=inherit
 
[Install]
WantedBy=multi-user.target
```

添加该服务

```sh
# 通知systemd一个新的单元文件存在
sudo systemctl daemon-reload

# 应用frpc服务
sudo systemctl enable --now frpc
```

服务管理:

```sh
# 检查服务状态
sudo systemctl status frpc
# 启动、停止、重启frpc
sudo systemctl start frpc
sudo systemctl stop frpc
sudo systemctl restart frpc
```

### 5.快速使用

首先我在`server1`上解析了一个域名:`frp.bnblogs.cc`，然后在宝塔面板上创建同名网站用来访问本地服务器上的`java`项目。

如果你有`SSL`证书,可以配置成`https`网站

<center>{{<image src="/images/all/image-20221012185737545.png" caption="">}}</center>

在宝塔面板中设置反向代理如下：

<center>{{<image src="/images/all/image-20221012184258795.png" caption="">}}</center>

接着启动`frps`服务就行了，服务端配置完成!

<center>{{<image src="/images/all/image-20221012184523697.png" caption="">}}</center>

接着在`server2`中操作(首先确定通过`server2`的`ip`能正常访问你的项目)

接下来配置`nginx`

将`default`配置复制一份到对应域名的配置

```sh
sudo cp /etc/nginx/sites-available/default  /etc/nginx/sites-available/frp.bnblogs.cc.conf
```

修改`frpc.ini`，添加下面的内容

```ini
[frp]
type = http
local_ip = server2的ip
local_port = 80
remote_port = 808
custom_domains = frp.bnblogs.cc 
```

启动`frp`客户端

<center>{{<image src="/images/all/image-20221012185452605.png" caption="">}}</center>

使用浏览器访问`https://frp.bnblogs.cc`，如果出现界面表示配置成功


<center>{{<image src="/images/all/image-20221012185609871.png" caption="">}}</center>



{{< admonition quote >}}
[1] [生活记录+电脑技术：今天搭建成功了frp服务，可以用外网访问内网网站了](https://www.taholab.com/25181)

[2] [电脑技术：在CentOS上将frp内网穿透服务注册为服务并开启自启动且失效自启动](https://www.taholab.com/25200)

[3] [使用frp内网穿透+宝塔面板实现外网无缝访问内网网站，简易、快速、可靠！](https://www.bilibili.com/video/BV1Le411M7oo/?spm_id_from=333.1007.top_right_bar_window_history.content.click&vd_source=0a13b720e7bcb002a4a6ad73658d7395)
{{< /admonition >}}
