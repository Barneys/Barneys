# git撤销操作


<!--more-->

#### 1.撤销已commit的文件

**撤销某个单独文件**：任选一种

```
git restore --staged 文件名 

git reset 文件名
```

**撤销所有add的文件**：任选一种

```
git reset HEAD 

git reset // 新版本可不加HEAD
```

#### 2.修改已提交的commit注释

```
git commit --amend -m "new commit"
```

#### 3.撤销未add前的所有修改

回滚到上一次提交的内容

```
git checkout 文件名
或者
git restore 文件名
```

#### 4.已push回到上一次提交的状态

```
git reset --hard HEAD^
或者
git reset --hard HEAD~
```

**更粗暴的方式:**直接撤销本次提交

```
git revert HEAD
```

上述命令含义是在当前提交后面，新增一次提交，抵消掉上一次提交导致的所有变化。

#### 5.回到任意的commit版本

使用`git reflog`可查看所有`HEAD`指针移动记录, 找到对应版本号

```
git reset --hard 版本号
```

#### 6.撤销时常用命令

1.查看文件修改状态

```
git status
```

<strong style='color:red'>红色</strong>:工作区修改了但是未加入暂存区的文件

<strong style='color:green'>绿色</strong>:已加入暂存区但未提交

2.查看暂存区文件

```
git ls-files
```
