# 


### 关于本站

本博客网站采用的是[DoIt 主题](https://github.com/HEIGE-PCloud/DoIt)，评论系统为`twikoo`，参考本博客的文章时请注明出处。

### 建站日志

{{< admonition info>}}
2022-03-01,博客从`gitee pages`转移到腾讯云服务器
{{< /admonition >}}

{{< admonition info>}}
2022-03-06,注册域名:`bnblogs.cc`,取自"<strong style="color:red">b</strong>ar<strong style="color:red">n</strong>ey's <strong style="color:red">blog</strong>"
{{< /admonition >}}

{{< admonition info>}}
2022-03-16,域名备案通过,博客地址改为:[https://hugo.bnblogs.cc](https://hugo.bnblogs.cc)
{{< /admonition >}}

{{< admonition info>}}
2022-03-18,公网备案通过
{{< /admonition >}}

{{< admonition info>}}
2022-03-19,本站已被 google 收录
{{< /admonition >}}

{{< admonition info>}}
2022-03-23,本站已被 bing 收录
{{< /admonition >}}

{{< admonition info>}}
2022-03-26,gitee 图床出现问题，已修复
{{< /admonition >}}

{{< admonition info>}}
2022-03-27,gitee 使用了防盗链，所有图片失效，已弃用
{{< /admonition >}}

{{< admonition info>}}
2022-04-05,接入百度统计，Google Analytics,加入[Travelling](https://travellings.link/)
{{< /admonition >}}

{{< admonition info>}}
2022-04-28,本站已被百度收录
{{< /admonition >}}

{{< admonition info>}}
2022-05-29,原主题[LoveIt](https://github.com/dillonzq/LoveIt)作者不再维护，改用[Doit](https://github.com/HEIGE-PCloud/DoIt)主题
{{< /admonition >}}

{{< admonition info>}}
2022-07-27,只公布前 20 篇文章的[rss 订阅](https://hugo.bnblogs.cc/index.xml)
{{< /admonition >}}

{{< admonition info>}}
2022-08-08,已加入[十年之约](https://www.foreverblog.cn/blog/3405.html)
{{< /admonition >}}

{{< admonition info>}}
2022-08-30,`waline`评论系统暂时无法访问，临时使用`utterances`
{{< /admonition >}}

{{< admonition info>}}
2022-08-30,`waline`评论系统已修复
{{< /admonition >}}

{{< admonition info>}}
2022-08-30,`waline`重新部署出现bug,临时使用`utterances`
{{< /admonition >}}

{{< admonition info>}}
2022-08-31, 更换评论系统`twikoo`
{{< /admonition >}}

{{< admonition info>}}
2022-10-01, 评论表情包和头像cravatar已修复
{{< /admonition >}}

### 关于博主

喜欢折腾的家伙

### 联系方式

如果有想法进一步交流，请在[留言板](https://hugo.bnblogs.cc/message/)留言，或者通过下面联系方式找到我

email：tzhangfp@163.com

微信/qq:

<table>
    <tr>
        <td>{{< image src="/images/img/20210722215940.png" caption="微信">}}
        <td>{{< image src="/images/img/20210722220052.jpg" caption="qq" width="55%">}}
    </tr>
</table>

