# curl使用

<!--more-->


**本文中所有请求的`api`用`url`代替**

#### 发送请求

```sh
curl url # GET请求,相当于curl -XGET url

curl -XPOST url -d 数据 -H 请求头 # POST请求,多个首部可以使用多个-H

curl -XPOST url -d 数据 # PUT请求

curl -XDELETE url # DELETE请求
```

#### 获取响应首部

```sh
curl -I url
```

<center>{{<image src="https://cdn.jsdelivr.net/gh/JokerZhang66/images@master//img/image-20221007183637368.png" caption="">}}</center>

#### 下载资源

```sh
curl -O url # 直接下载,大O
curl -o 文件名 url # 另存为其他文件名，小o
curl -C - url # 恢复下载
```

<center>{{<image src="https://cdn.jsdelivr.net/gh/JokerZhang66/images@master//img/image-20221007184123144.png" caption="">}}</center>

#### 跟随重定向

```sh
curl -L url
```

#### 查看更多信息

`-v` 参数表示显示一次 `http`通信的整个过程,包括端口连接和`http request`头信息

```sh
curl -v url
```

#### 使用代理访问网站

```sh
curl -x http://127.0.0.1:1217 https://www.youtube.com/

curl --proxy "http://127.0.0.1:1217" https://twitter.com/ # 或者使用--poroxy
```

#### ftp文件上传和下载

```sh
curl -u 用户名:密码 -O ftp文件地址 # 下载文件

curl -u 用户名:密码 -T 文件 ftp服务器地址 # 上传文件
```



