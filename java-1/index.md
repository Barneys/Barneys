# java核心技术(1)第3章-基本程序设计结构


<!--more-->

### 基本数据类型

```java
public class Main  {
    public static void main(String[]args) {
        // 2进制表示--10进制为3
        System.out.println(0b0011);
        // 16进制表示--10进制为14
        System.out.println(0xe);
        // 8进制表示--10进制为8
        System.out.println(010);
        // 可以在数字中加下划线便于阅读
        System.out.println(100_100_100);
        // 长整型可以加一个'L'作为后缀
        System.out.println(1000000000L);
        // float类型的浮点数后面使用后缀'F'，double类型的浮点数后面使用后缀'D'，
        System.out.println(0.01F);
        System.out.println(0.01D);

    }
}

```

### 数组深拷贝和浅拷贝

```java
import java.util.Arrays;

public class Main  {
    public static void main(String[]args) {
        int[] arr = {1,2,3,4,5};
        int[] cloneArr = arr;
        int[] p = Arrays.copyOf(arr,arr.length);
        arr[1] = 10;

        for (int num: arr) {
            System.out.printf(num + " ");
        }

        System.out.println();

        for (int num: cloneArr) {
            System.out.printf(num + " ");
        }

        System.out.println();

        for (int num: p) {
            System.out.printf(num + " ");
        }
    }
}
```

### 多维数组

每一行的数据长度可以不相等，创建多维数组时必须指定行数

```java
import java.util.Arrays;

public class Main  {
    public static void main(String[]args) {
        final int MAX = 10;
        // 指定行数
        int[][] arr = new int[MAX][];
        // 分配行的长度
        for (int i = 0; i < MAX; i++) {
            arr[i] = new int[i+1];
        }
        
        for (int i = 0; i < MAX; i++) {
            for (int j = 0; j <= i; j++) {
                arr[i][j] = j+1;
            }
        }
        for (int[] row : arr) {
            for (int val: row) {
                System.out.print(val + " ");
            }
            System.out.println();
        }
        
        // 快速打印二维数组的值
        System.out.println(Arrays.deepToString(arr));
    }
}
```

交换数组的任意两行

```java
import java.util.Arrays;

public class Main  {
    public static void main(String[]args) {
        int[][] arr = {{1,2,3},{4,5,6,7}};
        int[] tmp = arr[0];
        arr[0] = arr[1];
        arr[1] = tmp;
        System.out.println(Arrays.deepToString(arr));
    }
}
```

二维数组复制

```java
public class Main  {
    public static void main(String[]args) {
        int[][] arr = {{1,2,3},{4,5,6,7}};
        // 长度为行数
        int[][] arr_copy = Arrays.copyOf(arr,2);
        System.out.println(Arrays.deepToString(arr_copy));
    }
}
```

### 不可变字符串

```java
String ans = String.join(",","hello","are","you","ok"); // 使用逗号分隔字符串
System.out.println("hello".equalsIgnoreCase("HEllo")); // true 不区分大小写比较 
```

### 控制台程序

```java
import java.io.Console;

public class Main  {
    public static void main(String[]args) {
        Console console = System.console();
        String str = console.readLine("User name: ");
        char[] password = console.readPassword("password: ");
        System.out.println(str);
        for (int i = 0; i < password.length; i++) {
            System.out.println(password[i]);
        }
    }
}
```

### 格式化输出

对第`i`个参数多次不同的格式化输出

```java
import java.util.Date;

public class Main  {
    public static void main(String[]args) {
        String name = "Alice";
        int age = 18;
        String message = String.format("Hello %s,Next year you will be %d",name,age+1);
        System.out.println(message);
        System.out.printf("%.2f%%\n",20*100/100.0);
        System.out.printf("%1$s %2$tB %2$te, %2$tY\n", "Due date:", new Date());
        System.out.printf("%1$.2f,%1$.3f,%1$.4f",100/3.0);
    }
}
```

### 带标签的break

标签必须放在希望跳出的最外层循环之前， 并且必须紧跟一个冒号`:`

可以将一个标签放在想要跳到的语句块之前，类似于`goto`的作用

```java
import java.util.Scanner;

public class Main  {
    public static void main(String[]args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        label:
        for (int i = 0; i < n; i++) {
            if (i == 10) {
                System.out.println(i);
                break label;
            } else {
                System.out.println(i);
            }
        }
    }
}
```

### 大整数

```java
import java.math.BigInteger;

public class Main  {
    public static void main(String[]args) {
        // 使用字符串创建大整数
        BigInteger bigInteger = new BigInteger("11111111111111111111111111111111111111111111111111111111");
        // 最大只支持long
        BigInteger bigInteger1 = BigInteger.valueOf(111111111);
        // 四则运算+取模
        System.out.println(bigInteger.add(bigInteger1));
        System.out.println(bigInteger.subtract(bigInteger1));
        System.out.println(bigInteger.multiply(BigInteger.valueOf(1111111)));
        System.out.println(bigInteger.divide(new BigInteger("11111111")));
        System.out.println(bigInteger.mod(bigInteger1));
    }
}

```


