# waline评论迁移到twikoo


<!--more-->

### 评论系统版本

- [waline](https://github.com/walinejs/waline): v2.6.3

- [twikoo](https://twikoo.js.org/): v1.6.7

### 导出waline评论

进入`waline`管理后台(部署网址/ui)，导出保存的所有评论，会得到一个`waline.json`文件

<center>{{<image src="/images/all/waline_output.png" caption="导出">}}</center>

`waline.json`内容如下:

```json
{
	"type": "waline",
	"version": 1,
	"time": xxx,
	"tables": [
		"Comment",
		"Counter",
		"Users"
	],
	"data": {
		"Comment":[
            // 只需要这里的数据
            {xxx},
            {xxx},
        ],
        "Counter":{
            xxx
        },
        "Users":{
            xxx
        }
    }
}
```

由于和后面twikoo导入格式不匹配，所以需要稍作修改！我们只需要留下"Comment"中的内容，并作为"results"的值.

修改后的 json 内容如下:

```json
{
	"results": [
        // 导出的waline.json中的data字段中的"Comment"中的内容
        {xxx},
        {xxx},
    ]
}
```

保存修改后的`waline.json`文件

### 评论导入twikoo

选择`valine`源系统，上传修改后的`waline.json`文件

<center>{{<image src="/images/all/twikoo_input.png" caption="导入">}}</center>

不出意外的话，就导入成功了！

### 问题总结

导入评论后可能有这么两个问题:

1. 评论内容前有一些无用信息
2. 部分链接中的评论没有显示

重新查看`waline.json`文件，回复评论中会在评论内容前加入一些无关信息，我们要手动删除。没有显示的评论是由于评论数据保存的url中包含中文字符，导致解析失败。

<center>{{<image src="/images/all/error_comments.png" caption="导致错误的评论">}}</center>

对于中文路径解析的问题，我们可以进行[编码转换](https://tool.oschina.net/encode?type=4)

<center>{{<image src="/images/all/url_encode.png" caption="编码转换">}}</center>

不过，如果你已经导入了评论，那么除了先删除之前导入的错误评论再重新导入(**太麻烦，除非你评论不多**)外，还有一个办法，那就是修改部署 twikoo 中的数据库保存的评论数据.

我使用的是独立部署的方式，所有评论保存在`db.json.0`文件中，其他部署方式没有了解，可以查看[官网](https://twikoo.js.org/)。

<center>{{<image src="/images/all/db.png" caption="数据库文件">}}</center>

### 最后

本站为什么突然放弃了 waline,原因大致如下:

1. `waline`之前是部署在[vercel](https://vercel.com/)上,但是不太稳定，经常被墙。<del>虽然我后面知道怎么解决</del>。而且上周我重新部署正好碰上`vercel`更新，导致`waline`一直报 500，一气之下就换`twikoo`了
2. `leanCloud`使用体验极差，而且现在必须使用备案后的域名
3. 评论数据还是要自己管理比较踏实
4. `twikoo`的使用体验确实不错

