# Js异步处理的三种方式


<!--more-->

### js异步实现方式

#### 1.使用回调函数

```js
const fs = require('fs') // 引入文件系统模块

// 传入的参数是一个函数
function readFile(cb) {
    // readFile函数的第二个参数是一个回调函数
    fs.readFile('./data.json', function (err, data) {
        if (err) {
            console.log(err);
        }
        else {
            cb(data.toString());
        }
    })
}

// 读取文件之后打印数据(异步操作完成之后执行)
readFile((data) => {
    console.log(data);
});
```

#### 2.使用`promise`对象

```js
const fs = require("fs"); 

function readFile() {
	// Promise接收一个成功的回调函数和一个失败的回调函数
	return new Promise((resolve, reject) => {
		fs.readFile("./data.json", (err, data) => {
			if (err) {
				reject(err);
			} else {
				resolve(data.toString());
			}
		});
	});
}

readFile()
	.then((data) => { // 成功的回调函数
		console.log(data);
	})
	.catch((err) => { // 失败的回调函数
		console.log(err); 
	});

```

#### 3.`await`和`async`

```js
const fs = require('fs');


// 返回一个Promise对象
function readFile() {
	// Promise接收一个成功的回调函数和一个失败的回调函数
	return new Promise((resolve, reject) => {
		fs.readFile("./data.json", (err, data) => {
			if (err) {
				reject(err);
			} else {
				resolve(data.toString());
			}
		});
	});
}


// await必须放在async关键词修饰的function内
// await后面跟一个Promise对象
async function readFileAsync() {
    try {
        const result = await readFile(); // 等待Promise对象成功获取数据
        console.log(result);
    }catch (err) {
        console.log(err);
    }
}

readFileAsync();
```


