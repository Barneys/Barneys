# Gitea+drone搭建CICD环境


<!--more-->

**环境版本**

```sh
gitea:latest
drone:2
drone-runner-docker:linux-amd64
```

|     IP 地址     | 端口 |        所属服务        |
| :-------------: | :--: | :--------------------: |
| 192.168.153.131 | 9300 |   Gitea 网页管理服务   |
| 192.168.153.131 | 9200 | SSH、HTTP 下载代码服务 |
| 192.168.153.131 | 9080 |   drone-Server 服务    |
| 192.168.153.131 | 9030 |   drone-Runner 服务    |

#### 1.基于`docker-compose`搭建`gitea`

数据库使用`mysql-8.0`

```sh
mkdir gitea
cd gitea
vim docker-compose.yml
```

`docker-compose.yml`文件内容如下:

```yml
version: "3"

networks:
    gitea:
        external: false

volumes:
    gitea:
        driver: local

services:
    server:
        image: gitea/gitea:1.15.2
        container_name: gitea
        environment:
            - DB_TYPE=mysql
            - DB_HOST=db:3306
            - DB_NAME=gitea
            - DB_USER=gitea
            - DB_PASSWD=gitea
        restart: always
        networks:
            - gitea
        volumes:
            - ./gitea:/data
            - /etc/timezone:/etc/timezone:ro
            - /etc/localtime:/etc/localtime:ro
        ports:
            - "9300:3000"
            - "9200:22"
        depends_on:
            - db

    db:
        image: mysql:8
        restart: always
        environment:
            - MYSQL_ROOT_PASSWORD=gitea
            - MYSQL_USER=gitea
            - MYSQL_PASSWORD=gitea
            - MYSQL_DATABASE=gitea
        ports:
            - 9306:3306
        networks:
            - gitea
        volumes:
            - ./mysql:/var/lib/mysql
```

开始安装

```sh
docker compose up -d
```

<center>{{<image src="/images/all/image-20221101232839292.png" caption="">}}</center>

打开浏览器,输入`http://192.168.153.131:9300`

修改配置如下:

<center>{{<image src="/images/all/image-20221101232739072.png" caption="">}}</center>

设置管理员账号

<center>{{<image src="/images/all/image-20221101232927191.png" caption="">}}</center>

直接安装!

#### 2.`Gitea`配置`Drone`的`OAuth`授权应用

用于`drone`连接`Gitea`

依次点击**设置**、**应用**,输入`drone`的重定向`URI`

<center>{{<image src="/images/all/image-20221101233530394.png" caption="">}}</center>

点击**创建应用**

<center>{{<image src="/images/all/image-20221101233746513.png" caption="">}}</center>

`drone`是包含`server`和`runner`的，他们之间的通信需要进行加密，还需要通过如下方式生成一个共享密钥

```sh
openssl rand -hex 16
```

<center>{{<image src="/images/all/image-20221101234217265.png" caption="">}}</center>

配置后面的`drone`共需要下面的三个信息

```
客户端id: a2caedc4-a821-454f-a84a-d457e60548fa
客户端密钥: abydiVyAU07JqVTPmqRNoAH99xXktVsAK4TdpK6c9KTh
共享密钥: fc8071d5fcf829dce2106e10692e8ff3
```

#### 3.安装`drone-server`和`drone-runner`

`docker-compose.yml`配置文件如下：

开始安装:

```
docker compose up -d
```

<center>{{<image src="/images/all/image-20221101235004342.png" caption="">}}</center>

安装完成后打开浏览器,输入`http://192.168.153.131:9080`

<center>{{<image src="/images/all/image-20221101235152555.png" caption="">}}</center>

点击`continue`

<center>{{<image src="/images/all/image-20221101235224130.png" caption="">}}</center>

点击应用授权，注册管理员账户

<center>{{<image src="/images/all/image-20221101235406857.png" caption="">}}</center>

大功告成，后续会继续更新`CICD`操作。

