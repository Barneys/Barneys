# django入门

<!--more-->

#### 参考

{{< admonition quote >}}
[django4.1官方入门教程](https://docs.djangoproject.com/zh-hans/4.1/)
{{< /admonition >}}


{{<image src="/images/all/django_procedure.png" caption="django运行流程">}}

#### 创建一个django项目

项目名：`mydemo`

```sh
django-admin startproject mydemo
```

####  创建管理员账号

在`manage.py`所在目录下执行

```
python manage.py migrate
python manage.py createsuperuser
```

#### 启动项目

```
python manage.py runserver 0.0.0.0:8000
```

#### 安装pymysql

```
pip install pymysql
```

#### 更换数据库为mysql

在`__init__.py`中添加

```python
import pymysql
pymysql.install_as_MySQLdb()
```

修改`settings.py`

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # 数据库类型
        'NAME': '', # 数据库名
        'USER': '', # 数据库用户名
        'PASSWORD': '', # 密码
        'HOST': '', # 服务器地址，本地为127.0.0.1
        'PORT': '', # 端口
    }
}
```

重新迁移一下

```python
python manage.py migrate 
```

如果`pymysql`使用有问题，可以更换`mysqlclient`,安装命令如下

```python
pip install mysqlclient
```

`settings.py`配置中不用修改，将上述的``__init__.py``清空即可

#### 新建一个app

`app`的名字为`myapp`

```
python manage.py startapp myapp 
```

#### 打开一个shell

首先安装一个`ipython`

```python
pip install ipython
```

打开`ipython`

```
python manage.py shell
```

输出`myapp`创建的表的`sql`语句

```
python manage.py sqlmigrate myapp 0001
```

#### 如何修改数据库

”三步走策略“：

- 编辑 `models.py` 文件，改变模型。
- 运行 `python manage.py makemigrations` 为模型的改变生成迁移文件。
- 运行 `python manage.py migrate` 来应用数据库迁移。

#### Django修改时区

```python
TIME_ZONE = 'Asia/Shanghai' # 修改时区

USE_TZ = False # 注意改为False时区才生效
```

#### 定义一个数据库表

在`models.py`中定义一个类(对应数据库中的表)，每一个对象为对应表中一行记录

```
class Player(models.Model):
    id = models.AutoField('编号',primary_key=True)  #id自增
    username = models.CharField('用户名',max_length=20) # 变量名为列名，备注为'用户'
    password = models.CharField('密码',max_length=20)
    photo = models.CharField('头像',max_length=100,default='zzzzzzz')
    add_date = models.DateTimeField('保存日期', default=timezone.now)
    mod_date = models.DateTimeField('最后修改日期', auto_now=True)
```

在`admin.py`中进行注册

```
admin.site.register(Player) # 注册Player表
```

#### 数据库操作

1.根据定义的列名查找数据

`filter`的返回值为一个列表，每一项是一个`Player`对象、

`get`返回的是

```python
# Django为主键查询提供了一个缩写：pk.下面的语句和Question.objects.get(id=1)效果一样.
>>> Question.objects.get(pk=1)
# 按照id查找
Player.objects.filter(id=5)
# 按照username查找
Player.objects.filter(username='zzz')
```

2.根据条件进行查询

```python
# 按照photo的前缀进行查找
Player.objects.filter(photo__startswith='zz') # 查询photo列中以zz开头的行 
# 查找所有photo前缀不是'zz'的记录
Player.objects.exclude(photo__startswith='zz') # 查询photo列中以zz开头的行

# 查询当年添加的记录
cur_year = timezone.now().year
Player.objects.filter(add_date__year=cur_year) # __相当于.
Choice.objects.filter(question__question_text__startswith='W') # 多表查询
```

3.常用API

```python
Player.objects.count() # 记录总数
Player.objects.all() # 所有Player对象
Player.objects.values_list('username',flat=True) # 查看某字段的所有不同数据
```

#### 查看官方的User表

```python
from django.contrib.auth.models import User
print(User.objects.all())
```

#### 修改一个用户为管理员

在`shell`中操作

```python
user = User.objects.get(username='zfp')
user.is_superuser=True
user.set_password('123456')
user.save()
```

注意要在`admin`后台管理界面中将`Staff status`勾选上

#### django重新生成数据库表

1. 将app内migrations文件夹里除了`__init__.py`这个文件外的所有文件删除。
2. 登录数据库，找到`django_migrations`这个表，然后将表内对应的`app`下的`migrations`记录删除即可。
   或者直接删除库
3. 重新执行命令

```python
python manage.py makemigrations
python manage.py migrate
```

#### 枚举类型

```
# int ---> str
# 1对应female, 2对应male
place = models.IntegerChoices('place', 'female male')

# str ---> str
# 'A'对应'BEST'，'B'对应'Pass'，'C'对应'Failed'
GRADE = (
        ('A', 'Best'),
        ('B', 'Pass'),
        ('C', 'Failed'),
)

grade_level = models.CharField('成绩等级', choices=GRADE, max_length=1)
```

#### 查看django版本

```python
python3 -m django version
```

#### 引入静态文件

在项目根目录下建`static`文件夹用来保存所有静态文件。

修改`settings.py`引入`static`文件夹路径

```python
STATIC_URL = 'static/'
STATICFILES_DIRS=(
    os.path.join(BASE_DIR,'static'),
)
```

在`html`页面中引入图片

```html
# 在第一行加入下列语句
{% load static %}

# 引入static文件夹下的images文件夹的apple.png
<img src={% static 'images/apple.png' %}>
```

#### django设置添加一个自增id字段

在`settings.py`中添加以下内容

```
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
```

#### 后端解决跨域问题

```python
ALLOWED_HOSTS = ['127.0.0.1'] # 允许127.0.0.1访问

#将允许将cookie包含在跨站点HTTP请求中
CORS_ALLOW_CREDENTIALS = True
#添加允许执行跨站点请求的主机，为True，则将不使用白名单，并且将接受所有来源
CORS_ORIGIN_ALLOW_ALL = True
# 允许所有的请求头
CORS_ALLOW_HEADERS = ('*')
```

#### requirements.txt

导出本项目所有需要安装的模块

```python
pip freeze >requirements.txt
```

安装所有requirements.txt中的模块

```python
pip install -r requirements.txt
```

#### Vue3将css和js打包成一个文件

添加配置文件`vue.config.js`：
可以让`vue3`将项目打包成一个`js`文件和一个`css`文件。

```js
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    // No need for splitting
    optimization: {
      splitChunks: false
    }
  }
})
```


