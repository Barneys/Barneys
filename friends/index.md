# 

### 交个朋友吧

{{< admonition tip "提示" >}}
交换友链请使用下面的格式留言，欢迎大家来交换友链，看到就会添加的!
{{< /admonition >}}

- 网站名称：myblog
- 网站地址：https://myblog.com
- 网站头像：https://xxx.png
- 网站描述：早起的虫儿被鸟吃:joy:

{{< admonition >}}
友链会定期清理，如果出现没有互关、网站无法访问、内容违法等情况会立即删除！如有我忘记添加友链的小伙伴，请及时留言给我！

本次清理友链时间为 2022-10-12,预计三个月清理一次!
{{< /admonition >}}

### 我的信息

- 网站名称：Barney's Blog
- 网站地址：https://hugo.bnblogs.cc/
- 网站头像：下面任选一个
  - https://hugo.bnblogs.cc/images/img/20220215001349.png
  - https://cravatar.cn/avatar/a2bc729e1ee6040fff197c705e19d449?s=400&r=G&d=mp&ver=1664632345

- 网站描述：All the truth is simple
- 网站 RSS 地址(可选): https://hugo.bnblogs.cc/index.xml

{{< admonition tip "提示">}}
欢迎各位在申请友链时提供自己的 RSS 订阅地址
{{< /admonition >}}

### 友情链接

{{< friend name="Dillon" url="https://github.com/dillonzq/" logo="https://avatars0.githubusercontent.com/u/30786232?s=460&u=5fc878f67c869ce6628cf65121b8d73e1733f941&v=4" word="LoveIt主题作者" img-animation="rotate" primary-color="red">}}

{{< friend name="EmoryHuang" url="https://emoryhuang.cn/" logo="https://static.emoryhuang.cn/webp/emoryhuang-avatar.webp" word="Learning everything" >}}

{{< friend name="Gahotx" url="https://blog.gahotx.cn/" logo="https://pub.gahotx.cn/photo/cat.jpg" word="Don't repeat yourself" >}}

{{< friend name="雨临Lewis的博客" url="https://lewky.cn/" logo="https://cdn.jsdelivr.net/gh/lewky/lewky.github.io@master/images/avatar.jpg" word="不想当写手的码农不是好咸鱼_(xз」∠)_" >}}

{{< friend name="Hawkhai" url="https://www.sunocean.life/blog/" logo="https://www.sunocean.life/favicon.ico" word="Sun & Ocean">}}

{{< friend name="雷雷屋头" url="https://ll.sc.cn" logo="https://ll.sc.cn/images/avatar.webp" word="假装是个室内设计师的工地搬砖狗">}}

{{< friend name="飞雪无情的博客" url="https://www.flysnow.org/" logo="https://www.flysnow.org/favicon.ico" word="专注于Android、Java、Go语言(golang)、移动互联网、项目管理、软件架构" >}}

{{< friend name="李文周的博客" url="https://www.liwenzhou.com/" logo="https://www.liwenzhou.com/favicon.ico" word="总结Go语言学习之路，提供免费的Go语言学习教程，希望与大家一起学习进步" >}}

{{< friend name="进击的学霸的博客" url="https://blog.lovem.fun/" logo="https://qny.lovem.fun/pikaqiu.png" word="代码写出来是给人看的，附带能在机器上运行" >}}

{{< friend name="BBing's Blog" url="https://bbing.com.cn/" logo="https://cdn.jsdelivr.net/gh/caibingcheng/resources@main/images/2B2pd1.png" word="自由 分享 合作" >}}

{{< friend name="OhYee" url="https://www.ohyee.cc" logo="https://static.oyohyee.com/logo.svg" word="个人向笔记性质技术分享，共产主义开源编程🤪" >}}

{{< friend name="Roookie博客" url="https://www.wlplove.com" logo="https://image.wlplove.com/static/img/icon/headpic.png" word="记录 · 收纳 · 分享" >}}

{{< friend name="Eulaの小破站" url="https://www.eula.club/" logo="https://www.eula.club/images/avatar.webp" word="我不知道将去向何方，但我已在路上" >}}

{{< friend name="一只橙子" url="https://www.yzczi.com" logo="https://www.yzczi.com/wp-content/uploads/62d93fdfc6235947.png" word="网络资源分享的技术博客" >}}

{{< friend name="米米的博客" url="https://zhangshuqiao.org" logo="https://gravatar.loli.net/avatar/f56980e9dc026727282a04eea02b4f4d?s=512" word="米米的博客！！" >}}


{{< friend name="凡梦星尘空间站" url="https://lisenhui.cn" logo="https://lisenhui.cn/imgs/avatar.png" word="再平凡的人也有属于他的梦想！" >}}

{{< friend name="人家故里" url="https://fx7.top" logo="https://fx7.top/images/logo.png" word="不积跬步无以至千里，不积小流无以成江海" >}}


{{< friend name="浮云翩迁之间" url="https://blognas.hwb0307.com" logo="https://blognas.hwb0307.com/logo.jpg" word="百代繁华一朝都，谁非过客；千秋明月吹角寒，花是主人。" >}}


{{< friend name="ncc的个人网站" url="https://www.zqcnc.cn/" logo="https://www.zqcnc.cn/favicon.ico">}}

{{< friend name="泠泫凝的异次元空间" url="https://lxnchan.cn" logo="https://lxnchan.cn/assets/logo-v2.webp">}}

{{< friend name="Moyok的博客" url="https://blog.integer.top" logo="https://blog.integer.top/avatar" word="念念不忘，必有回响。">}}
