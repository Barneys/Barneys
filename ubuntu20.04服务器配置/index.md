# Ubuntu20.04服务器配置


<!--more-->

### 1.创建 root 用户

```
sudo passwd root
```

### 2.创建其他用户并赋予root权限

```sh
# 新建的用户名为user
sudo adduser user # 输入用户名密码
sudo usermod -aG sudo user # 赋予sudo权限

# 顺便在该用户家目录创建.ssh文件夹以及authorized_keys文件
cd
mkdir .ssh && chmod 700 .ssh
touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

### 3.更换清华源

```sh
sudo sed -i "s@http://.*archive.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
sudo sed -i "s@http://.*security.ubuntu.com@https://mirrors.tuna.tsinghua.edu.cn@g" /etc/apt/sources.list
```

### 4.修改时区

```sh
sudo timedatectl set-timezone Asia/Shanghai
```

### 5.配置静态 ip

查看 ip 地址,网卡等等

```sh
ip a
```

查看网关

```sh
sudo apt install net-tools

route -n
```

打开配置文件

```sh
sudo vim /etc/netplan/00-installer-config.yaml
```

修改文件内容如下(**注意缩进**):

```yaml
network:
    ethernets:
        ens33:
            dhcp4: no
            optional: true
            addresses: [192.168.236.130/24]
            gateway4: 192.168.236.2
            nameservers:
                addresses: [8.8.8.8, 114.114.114.114]
    version: 2
```

`ens33`网卡没有绑定 ipv4 地址，尝试查看服务有没有启动(**锐捷客户端会自动关闭 VMware NAT Service**)

<center>{{<image src="/images/all/20221009184208192.png" caption="">}}</center>

### 6.配置免密登录

本地生成 ssh 密钥

```
ssh-keygen
```

<center>{{<image src="/images/all/20221009220151.png" caption="">}}</center>

获取公钥

```
cat ~/.ssh/ubuntu_2_rsa.pub
```

存入服务器的`.ssh/authorized_keys`文件内

**更简单粗暴的方法:**

```
ssh-copy-id 服务器别名
```

### 7.重启 ssh 服务

```bash
sudo /etc/init.d/ssh restart
```

### 8.关闭防火墙

```sh
# 查看防火墙状态
sudo ufw status
# 开启防火墙
sudo ufw enable
# 关闭防火墙
sudo ufw disable
```

### 9.修改主机名

```sh
hostnamectl set-hostname newname
```

### 10.好用软件推荐

1.`ncdu`

可用来查看目录大小以及查看目录中的文件

安装命令

```sh
sudo apt install ncdu
```

使用方法: `ncdu+路径名`，按`q`退出，方向键切换目录

例如: `ncdu ~`

2.`htop`(自带)

查看系统资源使用状态

3.`ranger`

用于动态查看子目录及其所有文件，还可以预览文件内容以及选择用什么软件打开

安装命令

```sh
sudo apt install ranger
```

操作方式同`ncdu`

4.`neofetch`

简单地查看主机的硬件信息。

安装命令：

```sh
sudo apt install neofetch
```



### 11.安装开发常用工具

自带软件:

-   `git[v2.25.0]`

#### 1.安装 gcc

```sh
sudo apt install build-essential
gcc -v # 查看gcc版本
```

#### 2.安装 miniconda

安装`python3.8`对应版本

```
wget -c https://repo.anaconda.com/miniconda/Miniconda3-py38_4.12.0-Linux-x86_64.sh
```

添加权限

```
chmod 777 Miniconda3-py38_4.12.0-Linux-x86_64.sh
sh Miniconda3-py38_4.12.0-Linux-x86_64.sh
```

一直`Enter`,然后`yes`，最后确认一下安装路径

添加到环境变量,这里使用的是`zsh`

```
vim ~/.zshrc
export PATH=/home/user/miniconda3/bin:$PATH
```

#### 3.安装 oracle-jdk

安装之前最好卸载`openjdk`

下载地址:[https://sunyanos.github.io/2021/04/19/Ubuntu20-04LTS%E5%AE%89%E8%A3%85OpenJDK8-Oracle-JDK8/](https://sunyanos.github.io/2021/04/19/Ubuntu20-04LTS%E5%AE%89%E8%A3%85OpenJDK8-Oracle-JDK8/)

下载下面这个版本：

<center>{{<image src="/images/all/20221010001158.png" caption="">}}</center>

```sh
# 下载(注意不要用wget直接下载)
本地下载之后将压缩包上传至服务器

# 解压
sudo mkdir /usr/lib/jvm
sudo tar -zxvf jdk-8u261-linux-x64.gz -C /usr/lib/jvm

# 添加环境变量
vim ~/.zshrc
# Oracle JDK8 Envirenment
export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_341  ## 目录要换成自己解压的目录
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH

# 使环境变量立即生效
source ~/.zshrc

## 检查是否安装成功
java -verison
javac
```

<center>{{<image src="/images/all/20221010003136.png" caption="">}}</center>

#### 4.安装 node.js

官网下载地址:[https://nodejs.org/en/download/](https://nodejs.org/en/download/)

```sh
# 创建文件夹
cd /usr/local/
mkdir node
cd node

# 官网下载压缩包后上传至Ubuntu
# 解压
sudo tar -xJvf ~/node-v16.17.1-linux-x64.tar.xz -C /usr/local/node

# 添加环境变量
vim ~/.zshrc
export PATH=/usr/local/node/node-v16.17.1-linux-x64/bin:$PATH
source ~/.zshrc

# 验证安装是否成功
node -v
npm -v
```

<center>{{<image src="/images/all/20221010175726.png" caption="">}}</center>

#### 5.安装 maven

官网下载地址:[https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)

```sh
# 下载并解压
tar xzvf apache-maven-3.8.6-bin.tar.gz

# 修改路径, 将解压的文件夹放在/opt下
sudo cp -r ~/apache-maven-3.8.6 /opt

# 添加环境变量
vim ~/.zshrc
export PATH=/opt/apache-maven-3.8.6/bin:$PATH
source ~/.zshrc

# 验证安装是否成功
mvn -v
```

<center>{{<image src="/images/all/20221010181130454.png" caption="">}}</center>

配置阿里源

修改`/opt/maven/apache-maven-3.8.6/conf/settings.xml`,添加下列内容

```xml
<mirror>
<id>alimaven</id>
<name>aliyun maven</name>
<url>http://maven.aliyun.com/nexus/content/groups/public/</url>
<mirrorOf>central</mirrorOf>
</mirror>
```

#### 6.安装 MySQL8.0

```sh
# 安装
sudo apt install mysql-server

# 查看MySQL服务状态
sudo systemctl status mysql

# 设置root@localhost用户
sudo mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '你的密码';
FLUSH PRIVILEGES;

# 创建root@%用户,远程登录使用
create user 'root'@'%' identified with mysql_native_password by '你的密码';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;

# 查看创建的用户
use mysql;
select host,user from user;

# 尝试登陆
mysql -uroot -p # 输入你上面设置的密码

# 实现远程连接mysql

# 先停止服务
sudo service mysql stop
# 打开mysqld配置文件
sudo vim /etc/mysql/mysql.conf.d/mysqld.cnf
注释掉bind-address = 127.0.0.1
# 重启服务
sudo service mysql start

# 服务管理
sudo service mysql status # 查看服务状态
sudo service mysql start # 启动服务
sudo service mysql stop # 停止服务
sudo service mysql restart # 重启服务
```

<center>{{<image src="/images/all/20221010183136.png" caption="">}}</center>
<center>{{<image src="/images/all/20221010190400.png" caption="">}}</center>

#### 7.安装 redis

下载地址:[https://redis.io/download/](https://redis.io/download/)

```sh
# 创建文件夹
sudo mkdir /usr/local/redis/

# 安装
wget https://download.redis.io/redis-stable.tar.gz

tar -xzvf redis-stable.tar.gz
cd redis-stable
sudo make && make install

# 前台运行
redis-server

# 后台运行
# 修改redis.conf将daemonize设置为yes
# 重新使用redis配置(注意redis.conf的路径)
redis-server redis.conf

# 查看redis进程
ps -ef|grep redis

# 关闭redis服务
redis-cli shutdown

# 使用redis工具
redis-cli
```

<center>{{<image src="/images/all/20221010205337.png" caption="">}}</center>

配置远程登录

打开`redis.conf`,修改下面内容

```sh
# 注释下面这行
# bind 127.0.0.1 -::1
requirepass 你的密码
```

关闭 redis

```sh
redis-cli -a 密码 shutdown
```

<center>{{<image src="/images/all/20221010214147.png" caption="">}}</center>

<center>{{<image src="/images/all/20221010223142.png" caption="">}}</center>

redis 服务化

创建服务文件

```sh
sudo vim /etc/systemd/system/redis.service
sudo chmod +x /etc/systemd/system/redis.service
```

配置如下:

```
[Unit]
Description=redis-server
After=network.target

[Service]
Type=forking
# 改为自己的redis-server的位置和redis.conf位置
ExecStart=/usr/local/bin/redis-server /home/zfp/redis-stable/redis.conf
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

服务管理

```sh
# 通知systemd一个新的单元文件存在
sudo systemctl daemon-reload
# 设置开机自启动
sudo systemctl enable redis
# 检查服务状态
sudo systemctl status redis
# 启动、停止、重启tomcat
sudo systemctl start redis
sudo systemctl stop redis
sudo systemctl restart redis
```

#### 8.安装 tomcat

下载地址:[https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi)

```sh
# 建文件夹
sudo mkdir /usr/local/tomcat

# 将下载的压缩包解压到上面的目录
sudo tar -zxvf apache-tomcat-9.0.68.tar.gz -C /usr/local/tomcat/

# 使用root用户
cd /usr/local/tomcat/apache-tomcat-9.0.68/bin/
./startup.sh # 启动tomcat服务器
```

<center>{{<image src="/images/all/20221010224837.png" caption="">}}</center>

将`tomcat`服务化，使用`systemctl`来管理

首先新建`tomcat.service`单元文件

```sh
sudo vim /etc/systemd/system/tomcat.service
# 加上权限
sudo chmod 777 /etc/systemd/system/tomcat.service
```

写入下列内容

```sh
[Unit]
Description=Tomcat 9 servlet container
After=network.target

[Service]
Type=forking

User=zfp # 改为自己的用户

# 下面的信息和压缩包的位置对应
Environment="JRE_HOME=/usr/lib/jvm/jdk1.8.0_341/jre"
Environment="CLASSPATH=/usr/local/tomcat/apache-tomcat-9.0.68/bin/bootstrap.jar:/usr/local/tomcat/apache-tomcat-9.0.68/bin/tomcat-juli.jar"

Environment="CATALINA_BASE=/usr/local/tomcat/apache-tomcat-9.0.68"
Environment="CATALINA_HOME=/usr/local/tomcat/apache-tomcat-9.0.68"
Environment="CATALINA_TMPDIR=/usr/local/tomcat/apache-tomcat-9.0.68/temp"
Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"

ExecStart=/usr/local/tomcat/apache-tomcat-9.0.68/bin/startup.sh
ExecStop=/usr/local/tomcat/apache-tomcat-9.0.68/bin/shutdown.sh

[Install]
WantedBy=multi-user.target
```

服务管理

```sh
# 通知systemd一个新的单元文件存在
sudo systemctl daemon-reload
# 启用并且启动Tomcat服务
sudo systemctl enable --now tomcat
# 检查服务状态
sudo systemctl status tomcat
# 启动、停止、重启tomcat
sudo systemctl start tomcat
sudo systemctl stop tomcat
sudo systemctl restart tomcat
```

将`war`包放在`tomcat`安装文件夹的`webapps`

```sh
mv xxx.war /usr/local/tomcat/apache-tomcat-9.0.68/webapps
# 重启服务后该出现一个war包同名文件夹
sudo systemctl restart tomcat
```

将`tomcat`的根路径改为上述`war`包的产生的文件夹

```xml
# 打开server配置文件
sudo /usr/local/tomcat/apache-tomcat-9.0.68/conf/server.xml

# 填入以下内容
<Context path="/" docBase="/usr/local/tomcat/apache-tomcat-9.0.68/webapps/ruoyi-admin" reloadable="false"></Context>
```

<center>{{<image src="/images/all/20221011214512143.png" caption="">}}</center>

#### 9.安装 nginx

```sh
# 安装nginx
sudo apt update
sudo apt install nginx
# 查看运行状态
sudo systemctl status nginx

# 配置文件
sudo vim /etc/nginx/nginx.conf

# 测试nginx改动是否正确
sudo nginx -t

# 重启nginx服务
sudo systemctl restart nginx
```

```nginx
 # ruoyi项目后端转发路径
 location /prod-api/ {
                proxy_set_header Host $http_host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header REMOTE-HOST $remote_addr;
                proxt_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxt_pass http://192.168.153.131:8080/;
 }
```

-   所有的 Nginx 配置文件都在`/etc/nginx/`目录下。
-   主要的 Nginx 配置文件是`/etc/nginx/nginx.conf`。
-   为每个域名创建一个独立的配置文件，便于维护服务器。你可以按照需要定义任意多的 block 文件。
-   Nginx 服务器配置文件被储存在`/etc/nginx/sites-available`目录下。在`/etc/nginx/sites-enabled`目录下的配置文件都将被 Nginx 使用。
-   最佳推荐是使用标准的命名方式。例如，如果你的域名是`mydomain.com`，那么配置文件应该被命名为`/etc/nginx/sites-available/mydomain.com.conf`
-   如果你在域名服务器配置块中有可重用的配置段，把这些配置段摘出来，做成一小段可重用的配置。
-   Nginx 日志文件(access.log 和 error.log)定位在`/var/log/nginx/`目录下。推荐为每个服务器配置块，配置一个不同的`access`和`error`。
-   你可以将你的网站根目录设置在任何你想要的地方。最常用的网站根目录位置包括：
    -   `/home/<user_name>/<site_name>`
    -   `/var/www/<site_name>`
    -   `/var/www/html/<site_name>`
    -   `/opt/<site_name>`

#### 10.安装 docker

官方教程:[https://docs.docker.com/engine/install/ubuntu/](https://docs.docker.com/engine/install/ubuntu/)

```
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gp

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin

apt-cache madison docker-ce

sudo service docker start
```

测试：

```
sudo docker run hello-world
```

<center>{{<image src="/images/all/20221011012110.png" caption="">}}</center>

避免每次使用`docker`都要加`sudo`命令,运行下列命令

```sh
sudo usermod -aG docker $USER
```

**注: 该命令需要重新连接服务器才生效**

#### 11.安装 Mongodb4.4

安装完成之后配置文件位于`/etc/mongod.conf`

```sh
# 首先安装gnupg软件包
sudo apt-get install gnupg
# 导入包管理系统使用的公钥
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
# 添加MongoDB存储库
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
# 更新存储库
sudo apt-get update
# 使用以下命令安装MongoDB
sudo apt install mongodb-org
# 启用mongod服务
sudo systemctl start mongod.service
# 查看mongod服务状态
sudo systemctl status mongod
# 可通过下面的命令检查是否安装成功
mongo --eval 'db.runCommand({ connectionStatus: 1 })'
```

启动成功!

<center>{{<image src="/images/all/image-20221019234622188.png" caption="">}}</center>

**mongod 服务管理**

```sh
# 启用并且启动mongod服务
sudo systemctl enable --now mongod
# 检查服务状态
sudo systemctl status mongod
# 启动、停止、重启mongod
sudo systemctl start mongod
sudo systemctl stop mongod
sudo systemctl restart mongod
```

查看`mongodb`版本

```sh
mongo --version
```

进入`mongo shell`

```sh
mongo
```

**配置远程登录**

-   打开配置文件: `sudo vim /etc/mongod.conf`
-   将`bindIp: 127.0.0.1` 改为 `bindIp: 0.0.0.0`-
-   重启`mongd`服务

##### 给 Admin 数据库创建账户管理员

```
# 进入/切换数据库到admin中
use admin
# 创建账户管理员
db.createUser({
	user: "admin",
	pwd: "123456",
	roles: [
		{role: "userAdminAnyDatabase",db:"admin"}
	]
})
```

**创建超级管理员**

```
# 进入/切换数据库到admin中
use admin
# 创建超级管理员账号
db.createUser({
    user: "root",
    pwd: "123456",
    roles: [
    	{ role: "root", db: "admin" }
    ]
})
```

