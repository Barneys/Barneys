# Hugo创建个人网站并部署到vps


<!--more-->

{{< admonition info >}}
请先参考[上一篇](https://hugo.bnblogs.cc/个人云服务器搭建git服务器/)文章学习创建git用户并赋予root权限，设置免密登录以及在服务器上创建仓库。
{{< /admonition >}}

#### 写在之前

在`/etc/sudoers`添加`git`用户权限
```sh
sudo chmod +w /etc/sudoers
sudo vim /etc/sudoers
```
在`root ALL=(ALL:ALL) ALL`下面添加`git ALL=(ALL:ALL) ALL`,保存退出

取消`/etc/sudoers`写权限

```sh
sudo chmod -w /etc/sudoers
```

#### 1.使用`git`用户创建一个`blog`仓库

``` sh
su git # 切换到git用户
cd 
mkdir project/blog
cd project/blog
git init --bare blog.git
chomd -R 777 blog.git
```

#### 2.通过`root`用户新建保存`blog`内容文件夹

```sh
mkdir -p /var/www
cd /var/www
mkdir blog
```

#### 3.接着修改 `/var/www/blog`文件夹权限，给用户`git`赋予`/tmp`的操作权限

```sh
chown -R git:git /var/www/blog/
sudo chmod -R 777 /tmp
```

#### 4.设置 `git` 钩子脚本

**切换为 git 用户和文件夹，新建 `post-receive` 脚本文件**

```sh
su git
cd /home/git/project/blog/blog.git/hooks
vim post-receive
```

`post-receive`文件内容如下:

```sh
#!/bin/bash

GIT_REPO=/home/git/project/blog/blog.git
TMP_DIR_CLONE=/tmp/blog
PUBLIC_WWW=/var/www/blog
rm -rf ${TMP_DIR_CLONE}
git clone $GIT_REPO $TMP_DIR_CLONE
rm -rf $PUBLIC_WWW/*
cp -rf $TMP_DIR_CLONE/* $PUBLIC_WWW
```

添加可执行权限

```sh
chmod +x post-receive
```

#### 5.初始化本地仓库

进入`hugo`生成的`public`文件夹,初始化仓库，配置仓库信息，提交一次

```sh
cd public
git init 
git config user.name your_git_name
git config user.email your_git_email
git add -A
git commit -m 'first commit'
```

绑定远程仓库后再`push`

```sh
git remote add origin git@your_VPS_IP:/home/git/project/blog/blog.git
# 或者使用上一篇教程设置的别名
git remote add origin git_server:/home/git/project/blog/blog.git


git remote set-url origin ssh://git@your_VPS_IP:SSH_Port/home/git/project/blog/blog.git
# 或者使用别名
git remote set-url origin ssh://git_server:SSH_Port/home/git/project/blog/blog.git


git push -u origin master
```

通过上述操作,在本地`push`后，`public`文件夹的文件经过钩子脚本处理后会克隆一份到`/var/www/blog`中，后面就可以使用`nginx`配合域名来实现网站上线了。

#### 6.`nginx`配置

建议使用`宝塔面板`进行配置,最关键就是修改静态文件所在位置:`root /var/www/blog;`

```nginx
server
{
	...
    #禁止访问的文件或目录
    location ~ ^/(\.user.ini|\.htaccess|\.git|\.svn|\.project|LICENSE|README.md)
    {
        return 404;
    }
    
    #一键申请SSL证书验证目录相关设置
    location ~ \.well-known{
        allow all;
    }
    
    # location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
    # {
    #     expires      30d;
    #     error_log /dev/null;
    #     access_log /dev/null;
    # }
    
    # location ~ .*\.(js|css)?$
    # {
    #     expires      12h;
    #     error_log /dev/null;
    #     access_log /dev/null; 
    # }
    location ~* ^.+\.(ico|gif|jpg|jpeg|png)$ {
        root /var/www/blog;
        access_log   off;
        expires      1d;
    }
    
    location ~* ^.+\.(css|js|txt|xml|swf|wav)$ {
        root /var/www/blog;
        access_log   off;
        expires      10m;
    }
    
    location / {
        root /var/www/blog;
        if (-f $request_filename) {
        rewrite ^/(.*)$  /$1 break;
        }
    }
    
    location /nginx_status {
        stub_status on;
        access_log off;
    }
    ...
}
```


